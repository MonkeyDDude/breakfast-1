<?php

namespace AppBundle\Form\Type;

use Admingenerator\FormExtensionsBundle\Form\Type\DatePickerType;
use Admingenerator\FormExtensionsBundle\Form\Type\Select2EntityType;
use AppBundle\Entity\Participant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class BreakfastDoneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('participant1', Select2EntityType::class, [
                'choice_label' => 'name',
                'class' => Participant::class
            ])
            ->add('participant2', Select2EntityType::class, [
                'choice_label' => 'name',
                'class' => Participant::class
            ])
            ->add('at', DatePickerType::class)
            ->add('save', SubmitType::class)
        ;
    }
}
