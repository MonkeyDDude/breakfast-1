<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Participant;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class ParticipantRepository extends EntityRepository
{
    /**
     * @param \DateTime $day
     *
     * @return Participant[]
     */
    public function getNextParticipants(\DateTime $day)
    {
        return $this
            ->createQueryBuilder('p')
            ->leftJoin('p.vacationSlots', 's', Join::WITH, 's.at = :day')
            ->where('s.id is null')
            ->addOrderBy('p.lastParticipatedAt', 'ASC')
            ->addOrderBy('p.id', 'ASC')
            ->setParameter('day', $day)
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();
    }
}
